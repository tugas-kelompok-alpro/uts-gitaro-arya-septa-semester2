package com.mycompany.kalkulatordowhile;

import java.util.Scanner;

public class KalkulatorDoWhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char operator;
        double hasil;

        do {
            System.out.print("Masukkan nilai pertama: ");
            double a = input.nextDouble();

            System.out.print("Pilih operator +, -, *, atau / (ketik 'q' untuk keluar): ");
            operator = input.next().charAt(0);

            if (operator == 'q') {
                break;
            }

            System.out.print("Masukkan nilai kedua: ");
            double b = input.nextDouble();

            switch (operator) {
                case '+':
                    hasil = a + b;
                    break;
                case '-':
                    hasil = a - b;
                    break;
                case '*':
                    hasil = a * b;
                    break;
                case '/':
                    if (b != 0) {
                        hasil = a / b;
                    } else {
                        System.out.println("Error: Pembagian dengan nol tidak valid.");
                        continue;
                    }
                    break;
                default:
                    System.out.println("Error: Operator yang Anda masukkan tidak valid.");
                    continue;
            }

            System.out.println("Hasil perhitungan: " + a + " " + operator + " " + b + " = " + hasil);
        } while (true);
    }
}
