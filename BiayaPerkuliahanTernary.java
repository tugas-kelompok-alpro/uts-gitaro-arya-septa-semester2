import java.util.Scanner;

public class BiayaPerkuliahanTernary {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double biayaPerSKS = 75000.0;
        int totalSKS = 24;

        System.out.print("Masukkan jumlah SKS yang akan dibayar: ");
        int sksBayar = input.nextInt();

        double totalBiaya = sksBayar * biayaPerSKS;

        double denda = (sksBayar < totalSKS) ? ((totalSKS - sksBayar) * 5000.0) : 0.0;

        System.out.println("Total biaya + denda: Rp " + (totalBiaya + denda));
    }
}