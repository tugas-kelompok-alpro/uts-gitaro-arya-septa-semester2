package com.mycompany.kalkulatorifelseelseif;

import java.util.Scanner;

public class Kalkulatorifelseelseif {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("==============================================");
        System.out.println("=          Program Kalkulator if-else-(elseif)     =");
        System.out.println("==============================================");

        System.out.print("Masukkan nilai pertama: ");
        float a = input.nextFloat();

        System.out.print("Pilih operator +, -, *, atau /: ");
        char operator = input.next().charAt(0);

        System.out.print("Masukkan nilai kedua: ");
        float b = input.nextFloat();

        float hasil;

        if (operator == '+') {
            hasil = a + b;
        } else if (operator == '-') {
            hasil = a - b;
        } else if (operator == '*') {
            hasil = a * b;
        } else if (operator == '/') {
            if (b != 0) {
                hasil = a / b;
            } else {
                System.out.println("Error: Pembagian dengan nol tidak valid.");
                return;
            }
        } else {
            System.out.println("Error: Operator yang Anda masukkan tidak valid.");
            return;
        }

        System.out.println("Hasil perhitungan: " + a + " " + operator + " " + b + " = " + hasil);
    }
}